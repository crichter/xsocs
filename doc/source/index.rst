
Welcome to X-SOCS's documentation!
==================================

The X-ray Strain Orientation Calculation Software (X-SOCS) is
a user-friendly software, developed for automatic analysis of
5D sets of data recorded during continuous mapping measurements.
X-SOCS aims at retrieving strain and tilt maps of nanostructures,
films, surfaces or even embedded structures.

Contents:

.. toctree::
    :hidden:

    overview.rst
    install.rst
    using.rst
    tutorials.rst
    faq.rst

:doc:`overview`
    Repository, issue tracker, ...

:doc:`install`
    How to install *X-SOCS* on Linux

:doc:`using`
    How to use *X-SOCS* to reduce KMAP data

:doc:`tutorials`
   Tutorials

:doc:`faq`
   Frequently Asked Questions, known limitations, ...

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

