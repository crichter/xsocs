
Installation
============

*X-SOCS* supports `Python <https://www.python.org/>`_ versions 2.7, 3.4 and 3.5.

At the moment *X-SOCS* can only be installed from the source, see `Installing from source`_.

Dependencies
------------

The dependencies of *X-SOCS* are:

* `silx <http://pythonhosted.org/silx/install.html>`_ and its `optional dependencies <http://pythonhosted.org/silx/install.html#dependencies>`_:
    * `matplotlib <http://matplotlib.org/>`_
    * `h5py <http://docs.h5py.org/en/latest/build.html>`_ for HDF5 files
    * A Qt binding: either `PyQt5 or PyQt4 <https://riverbankcomputing.com/software/pyqt/intro>`_
    * `PyOpenGL <http://pyopengl.sourceforge.net/>`_
* `xrayutilities <https://xrayutilities.sourceforge.io/>`_.
* `scipy <https://pypi.python.org/pypi/scipy>`_.

Build dependencies
++++++++++++++++++

The build dependencies for *X-SOCS* are the same as the ones for `silx <http://pythonhosted.org/silx/install.html#build-dependencies>`_::

    In addition to run-time dependencies, building *silx* requires a C/C++ compiler, numpy and cython (optional).

    On Windows it is recommended to use Python 3.5, because with previous versions of Python, it might be difficult to compile the extensions.

    This project uses cython to generate C files.
    Cython is not mandatory to build *silx* and is only needed when developing binary modules.
    If using cython, *silx* requires at least version 0.18 (with memory-view support).

Installing from source
----------------------

Building *X-SOCS* from the source requires some `Build dependencies`_.

Building from source
++++++++++++++++++++

Download the source from the `gitlab repository <https://gitlab.esrf.fr/kmap/xsocs.git>`_::

    git clone https://gitlab.esrf.fr/kmap/xsocs.git

Then cd into xsocs::

    cd xsocs

And install xsocs, either system-wide (requires root privileges)::

    pip install .

or for the current user only::

    pip install . --user

