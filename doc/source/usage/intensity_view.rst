
.. _intensity_view:

Converting to Q-Space (Intensity view)
======================================

.. |intensity_view| image:: img/intensity_view.png
.. |draw_roi_button| image:: img/draw_roi_button.png
.. |to_qspace_button| image:: img/to_qspace_button.png


+------------------+
| Intensity view   |
+==================+
| |intensity_view| |
+------------------+

This view display the total acquired intensity over the sample.
When an entry is selected in the list on the left, each point on the scatter
plot represents the total intensity of the acquired image at that point.

When the root node (labeled ``Intensity``) is selected, the total intensity of
all entries is displayed.

The list on the left allows you to select the entries that will be used when
converting to q-space.

You can select points on the central scatter plot to display an intensity
profile at that point at the bottom of the window.

Roi selection
.............

Before starting the conversion, a ROI (Region Of Interest) must be selected
on the scatter plot. To draw a ROI, click on the |draw_roi_button| icon on the
right and then select an area on the scatter plot.

When this is done, click on the |to_qspace_button| to open the :ref:`conversion window <conversion_window>`.

Conversion parameters
.....................

Please see the :ref:`conversion window <conversion_window>` documentation.

When the conversion is done, a new :ref:`qspace item <qspace_item>` is
added to the :ref:`project tree <project_tree>`. You can view the result in
the :ref:`qspace view <qspace_view>`.
