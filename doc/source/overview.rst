Overview
========

Releases
--------

TBD

Project
-------

- `Homepage <https://gitlab.esrf.fr/kmap/xsocs>`_
- `Source repository <https://gitlab.esrf.fr/kmap/xsocs>`_
- `Issue tracker <https://gitlab.esrf.fr/kmap/xsocs/issues>`_


