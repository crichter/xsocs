#!/usr/bin/python
# coding: utf8
# /*##########################################################################
#
# Copyright (c) 2015-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__date__ = "01/01/2017"
__license__ = "MIT"


import numpy as np

from .Plotter import Plotter

from ..process.fit.Fitter import Fitter
from ..process.fit.fitresults import FitStatus
from ..process.fit.sharedresults import FitSharedResults
from ..process.fit.fitresults import FitResult


class CentroidFitter(Fitter):
    def fit(self, i_fit, i_cube, qx_profile, qy_profile, qz_profile):

        zSum = qz_profile.sum()
        if sum != 0:
            com = self._qz.dot(qz_profile) / zSum
            idx = np.abs(self._qz - com).argmin()
            i_max = qz_profile.max()
            self._shared_results.set_qz_results(i_fit,
                                                [qz_profile[idx], com, i_max],
                                                FitStatus.OK)
        else:
            self._shared_results.set_qz_results(i_fit,
                                                [np.nan, np.nan, np.nan],
                                                FitStatus.FAILED)

        ySum = qy_profile.sum()
        if ySum != 0:
            com = self._qy.dot(qy_profile) / ySum
            idx = np.abs(self._qy - com).argmin()
            i_max = qy_profile.max()
            self._shared_results.set_qy_results(i_fit,
                                                [qy_profile[idx], com, i_max],
                                                FitStatus.OK)
        else:
            self._shared_results.set_qy_results(i_fit,
                                                [np.nan, np.nan, np.nan],
                                                FitStatus.FAILED)

        xSum = qx_profile.sum()
        if xSum != 0:
            com = self._qx.dot(qx_profile) / xSum
            idx = np.abs(self._qx - com).argmin()
            i_max = qx_profile.max()
            self._shared_results.set_qx_results(i_fit,
                                                [qx_profile[idx], com, i_max],
                                                FitStatus.OK)
        else:
            self._shared_results.set_qx_results(i_fit,
                                                [np.nan, np.nan, np.nan],
                                                FitStatus.FAILED)


class CentroidResults(FitSharedResults):
    def __init__(self,
                 n_points=None,
                 shared_results=None,
                 shared_status=None,
                 **kwargs):
        super(CentroidResults, self).__init__(n_points=n_points,
                                              n_params=3,
                                              n_peaks=1,
                                              shared_results=shared_results,
                                              shared_status=shared_status)

    def fit_results(self, *args, **kwargs):
        qx_results = self._npy_qx_results
        qy_results = self._npy_qy_results
        qz_results = self._npy_qz_results

        qx_status = self._npy_qx_status
        qy_status = self._npy_qy_status
        qz_status = self._npy_qz_status

        fit_name = 'Centroid'
        results = FitResult(fit_name, *args, **kwargs)

        results.add_qx_result('centroid', 'I', qx_results[:, 0].ravel())
        results.add_qx_result('centroid', 'COM',
                              qx_results[:, 1].ravel())
        results.add_qx_result('centroid', 'Max',
                              qx_results[:, 2].ravel())
        results.set_qx_status(qx_status)

        results.add_qy_result('centroid', 'I', qy_results[:, 0].ravel())
        results.add_qy_result('centroid',
                              'COM',
                              qy_results[:, 1].ravel())
        results.add_qy_result('centroid', 'Max',
                              qy_results[:, 2].ravel())
        results.set_qy_status(qy_status)

        results.add_qz_result('centroid', 'I', qz_results[:, 0].ravel())
        results.add_qz_result('centroid',
                              'COM',
                              qz_results[:, 1].ravel())
        results.add_qz_result('centroid', 'Max',
                              qz_results[:, 2].ravel())
        results.set_qz_status(qz_status)

        return results


class CentroidPlotter(Plotter):
    def plotFit(self, plot, x, peakParams):
        plot.setGraphTitle('center of mass')
        for peakName, peak in peakParams.items():
            center = peak.get('COM')

            if np.isfinite(center):
                plot.addXMarker(center, legend='center of mass')

    def getPlotTitle(self):
        return 'Center Of Mass'

# process = fitH5.processes(entry)[0]
#
#     positions = fitH5.get_result(entry, process, 'COM')
#
#     plots[0].addCurve(xAcqQX, yAcqQX, legend='measured')
#     plots[0].addXMarker(positions.qx[index], legend='center of mass')
#     plots[0].setGraphTitle('QX center of mass')
#
#     plots[1].addCurve(xAcqQY, yAcqQY, legend='measured')
#     plots[1].addXMarker(positions.qy[index], legend='center of mass')
#     plots[1].setGraphTitle('QY center of mass')
#
#     plots[2].addCurve(xAcqQZ, yAcqQZ, legend='measured')
#     plots[2].addXMarker(positions.qz[index], legend='center of mass')
#     plots[2].setGraphTitle('QZ center of mass')
#


if __name__ == '__main__':
    pass
