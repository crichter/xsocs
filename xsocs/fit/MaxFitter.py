#!/usr/bin/python
# coding: utf8
# /*##########################################################################
#
# Copyright (c) 2015-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__date__ = "01/01/2017"
__license__ = "MIT"


from .Plotter import Plotter

from ..process.fit.Fitter import Fitter
from ..process.fit.fitresults import FitStatus
from ..process.fit.sharedresults import FitSharedResults
from ..process.fit.fitresults import FitResult


class MaxFitter(Fitter):
    """
    Fitter that returns the maximum and its position.
    """
    def fit(self, i_fit, i_cube, qx_profile, qy_profile, qz_profile):
        idx_max = qx_profile.argmax()
        self._shared_results.set_qx_results(i_fit,
                                            [qx_profile[idx_max],
                                             self._qx[idx_max]],
                                            FitStatus.OK)

        idx_max = qy_profile.argmax()
        self._shared_results.set_qy_results(i_fit,
                                            [qy_profile[idx_max],
                                             self._qy[idx_max]],
                                            FitStatus.OK)

        idx_max = qz_profile.argmax()
        self._shared_results.set_qz_results(i_fit,
                                            [qz_profile[idx_max],
                                             self._qz[idx_max]],
                                            FitStatus.OK)


class MaxResults(FitSharedResults):
    def __init__(self,
                 n_points=None,
                 shared_results=None,
                 shared_status=None):
        super(MaxResults, self).__init__(n_points=n_points,
                                         n_params=2,
                                         n_peaks=1,
                                         shared_results=shared_results,
                                         shared_status=shared_status)

    def fit_results(self, *args, **kwargs):
        qx_results = self._npy_qx_results
        qy_results = self._npy_qy_results
        qz_results = self._npy_qz_results

        qx_status = self._npy_qx_status
        qy_status = self._npy_qy_status
        qz_status = self._npy_qz_status

        fit_name = 'Max'
        results = FitResult(fit_name, *args, **kwargs)

        results.add_qx_result('max', 'max', qx_results[:, 0].ravel())
        results.add_qx_result('max', 'position',
                              qx_results[:, 1].ravel())
        results.set_qx_status(qx_status)

        results.add_qy_result('max', 'max', qy_results[:, 0].ravel())
        results.add_qy_result('max', 'position',
                              qy_results[:, 1].ravel())
        results.set_qy_status(qy_status)

        results.add_qz_result('max', 'max', qz_results[:, 0].ravel())
        results.add_qz_result('max', 'position',
                              qz_results[:, 1].ravel())
        results.set_qz_status(qz_status)

        return results


class MaxPlotter(Plotter):

    def plotFit(self, plot, x, peakParams):
        plot.setGraphTitle('Maximum')
        for peakName, peak in peakParams.items():
            maximum = peak.get('max')
            position = peak.get('position')
            plot.addXMarker(position, legend='max_position')

    def getPlotTitle(self):
        return 'Maximum'


if __name__ == '__main__':
    pass
