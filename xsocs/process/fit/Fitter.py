#!/usr/bin/python
# coding: utf8
# /*##########################################################################
#
# Copyright (c) 2015-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__date__ = "01/01/2017"
__license__ = "MIT"


class Fitter(object):

    qx = property(lambda self: self._qx)
    """ qx axis values """

    qy = property(lambda self: self._qy)
    """ qy axis values """

    qz = property(lambda self: self._qz)
    """ qz axis values """

    shared_results = property(lambda self: self._shared_results)
    """ FitSharedResults instance use by this fitter """

    def __init__(self, qx, qy, qz,
                 shared_results):
        super(Fitter, self).__init__()

        self._shared_results = shared_results
        self._qx = qx
        self._qy = qy
        self._qz = qz

    def fit(self, i_fit, i_cube, qx_profile, qy_profile, qz_profile):
        """
        Performs the fit and stores the result in this instance's
        FitSharedResult object.
        :param i_fit: index of the fit result (in the FitH5 file). This
            index is to be used when storing the result in the FitSharedResult
            instance.
        :param i_cube: index of the cube (in the QSpaceH5 file)
        :param qx_profile: qx profile
        :param qy_profile: qy profile
        :param qz_profile: qz profile
        :return:
        """
        raise NotImplementedError('Not implemented.')


if __name__ == '__main__':
    pass
