# coding: utf-8
# /*##########################################################################
# Copyright (C) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ############################################################################*/
"""
Nominal tests for the Fitter class.
"""

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "05/01/2016"

import os
import shutil
import tempfile
import unittest

import numpy as np

from silx.test.utils import ParametricTestCase

from xsocs.test.testfilesmanager import TestFilesManager

from xsocs.process.fit.peak_fit import PeakFitter
from xsocs.io.FitH5 import FitH5


# ==============================================================
# ==============================================================
# ==============================================================

def _cmp_fit_h5_files(test_case, ref_h5, this_h5):
    """
    Compares two FitH5 files.
    :param test_case:
    :param ref_h5:
    :param this_h5:
    :return:
    """

    ref_entries = ref_h5.entries()
    this_entries = this_h5.entries()

    test_case.assertEqual(ref_entries, this_entries)

    for entry in ref_entries:
        _cmp_fit_h5_processes(test_case, ref_h5, this_h5, entry)


def _cmp_fit_h5_processes(test_case, ref_h5, this_h5, entry):
    """
    Compares two FitH5 files
    :param test_case:
    :param ref_h5:
    :param this_h5:
    :param entry:
    :return:
    """

    ref_processes = ref_h5.processes(entry)
    this_processes = this_h5.processes(entry)
    test_case.assertEqual(ref_processes, this_processes)

    for process in ref_processes:
        _cmp_fit_h5_results(test_case,
                            ref_h5,
                            this_h5,
                            entry,
                            process)


def _cmp_fit_h5_results(test_case,
                        ref_h5,
                        this_h5,
                        entry,
                        process):
    """
    Compares two FitH5 files
    :param test_case:
    :param ref_h5:
    :param this_h5:
    :param entry:
    :param process:
    :return:
    """

    ref_results = ref_h5.get_result_names(
        entry, process)
    this_results = this_h5.get_result_names(
        entry, process)

    test_case.assertEqual(ref_results, this_results)

    for result in ref_results:
        ref_qx = ref_h5.get_qx_result(entry, process, result)
        this_qx = this_h5.get_qx_result(entry, process, result)
        test_case.assertTrue(np.array_equal(ref_qx, this_qx))

        ref_qy = ref_h5.get_qy_result(entry, process, result)
        this_qy = this_h5.get_qy_result(entry, process, result)
        test_case.assertTrue(np.array_equal(ref_qy, this_qy))

        ref_qz = ref_h5.get_qz_result(entry, process, result)
        this_qz = this_h5.get_qz_result(entry, process, result)
        test_case.assertTrue(np.array_equal(ref_qz, this_qz))


class TestPeakFitter(ParametricTestCase):
    """
    Unit tests of the qspace converter class.
    """

    @classmethod
    def setUpClass(cls):
        cls._tmpdir = tempfile.mkdtemp()
        manager = cls._manager = TestFilesManager('www.silx.org/pub/'
                                                  'xsocs/test_data/')
        manager.checksum_lifetime = 0
        manager.get_files('qspace')
        manager.get_files('fit')

    @classmethod
    def tearDownClass(cls):
        tmpdir = cls._tmpdir
        if tmpdir is not None:
            shutil.rmtree(tmpdir)
        cls._tmpdir = None

    def setUp(self):
        self._tmpTestDir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self._tmpTestDir)
        self._tmpTestDir = None

    def test_nominal(self):
        """
        """

        manager = self._manager

        keys = ['fit_f', 'qspace_f']
        parameters = [
            ('gaussian_1.h5', 'qspace_1.h5'),
            ('gaussian_2.h5', 'qspace_2.h5'),
            ('gaussian_3.h5', 'qspace_3.h5'),
            ('gaussian_4.h5', 'qspace_4.h5')
        ]
        param_dicts = [dict(zip(keys, params)) for params in parameters]

        for params in param_dicts:

            with self.subTest(**params):

                qspace_f = manager.get_file('qspace/{0}'
                                            ''.format(params['qspace_f']))
                fit_out = os.path.join(self._tmpTestDir, params['fit_f'])

                fitter = PeakFitter(qspace_f)

                self.assertEqual(fitter.status, fitter.READY)

                fitter.peak_fit()

                self.assertEqual(fitter.status, fitter.DONE)

                results = fitter.results

                results.to_fit_h5(fit_out)

                fit_ref = manager.get_file('fit/{0}'
                                           ''.format(params['fit_f']))

                fit_ref_h5 = FitH5(fit_ref)
                fit_out_h5 = FitH5(fit_out)

                _cmp_fit_h5_files(self, fit_ref_h5, fit_out_h5)


# ==============================================================
# ==============================================================
# ==============================================================


test_cases = (TestPeakFitter
              ,)


def suite():
    loader = unittest.defaultTestLoader
    test_suite = unittest.TestSuite()
    for test_class in test_cases:
        tests = loader.loadTestsFromTestCase(test_class)
        test_suite.addTests(tests)
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
