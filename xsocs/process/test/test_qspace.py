# coding: utf-8
# /*##########################################################################
# Copyright (C) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ############################################################################*/
"""
Nominal tests for the KmapMerger class.
"""

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "05/01/2016"

import os
import shutil
import tempfile
import unittest

import numpy as np

from silx.test.utils import ParametricTestCase

from xsocs.test.testfilesmanager import TestFilesManager

from xsocs.io.QSpaceH5 import QSpaceH5
from xsocs.process.qspace.QSpaceConverter import QSpaceConverter


# ==============================================================
# ==============================================================
# ==============================================================


class TestQSpace(ParametricTestCase):
    """
    Unit tests of the qspace converter class.
    """

    @classmethod
    def setUpClass(cls):
        cls._tmpdir = tempfile.mkdtemp()
        manager = cls._manager = TestFilesManager('www.silx.org/pub/'
                                                  'xsocs/test_data/')
        manager.checksum_lifetime = 0
        manager.get_files('merged')
        manager.get_files('qspace')

    @classmethod
    def tearDownClass(cls):
        tmpdir = cls._tmpdir
        if tmpdir is not None:
            shutil.rmtree(tmpdir)
        cls._tmpdir = None

    def setUp(self):
        self._tmpTestDir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self._tmpTestDir)
        self._tmpTestDir = None

    def test_nominal(self):
        """
        """
        manager = self._manager

        master_f = manager.get_file('merged/test.h5')

        keys = ['output_f', 'img_binning', 'medfilt_dims']
        parameters = [
            ('qspace_1.h5', None, None),
            ('qspace_2.h5', [4, 4], None),
            ('qspace_3.h5', None, [3, 3]),
            ('qspace_4.h5', [4, 4], [3, 3]),
        ]
        param_dicts = [dict(zip(keys, params)) for params in parameters]

        for params in param_dicts:
            with self.subTest(**params):
                output_f = os.path.join(self._tmpTestDir, params['output_f'])

                converter = QSpaceConverter(master_f,
                                            qspace_dims=[28, 154, 60],
                                            img_binning=params['img_binning'],
                                            medfilt_dims=params['medfilt_dims'],
                                            output_f=output_f)
                self.assertEqual(converter.status, converter.READY)

                converter.convert()

                self.assertEqual(converter.status,
                                 converter.DONE,
                                 msg=converter.status_msg)

                q_ref = manager.get_file('qspace/{0}'
                                         ''.format(params['output_f']))
                q_out = converter.results

                q_ref_h5 = QSpaceH5(q_ref)
                q_out_h5 = QSpaceH5(q_out)

                with q_ref_h5.qspace_dset_ctx() as ref_ctx:
                    with q_out_h5.qspace_dset_ctx() as out_ctx:
                        self.assertEqual(ref_ctx.shape, out_ctx.shape)
                        self.assertEqual(ref_ctx.dtype, out_ctx.dtype)
                        self.assertTrue(np.array_equal(ref_ctx.shape,
                                                       out_ctx.shape))


# ==============================================================
# ==============================================================
# ==============================================================


test_cases = (TestQSpace,
              )


def suite():
    loader = unittest.defaultTestLoader
    test_suite = unittest.TestSuite()
    for test_class in test_cases:
        tests = loader.loadTestsFromTestCase(test_class)
        test_suite.addTests(tests)
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
