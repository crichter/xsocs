# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "01/11/2016"


import os

import numpy as np

from silx.gui import qt as Qt


from .ShiftSubject import ShiftSubject

from ...view.intensity.RectRoiWidget import RectRoiWidget

from ...widgets.Containers import GroupBox
from ...widgets.XsocsPlot2D import XsocsPlot2D
from ...widgets.PlotGrabber import PlotGrabber

from ...model.Node import Node, ModelDataList
from ...model.TreeView import TreeView
from ...model.Model import Model, RootNode
from ...model.ModelDef import ModelRoles, ModelColumns


class ShiftRootNode(RootNode):
    """
    Root node for the ShiftModel
    """
    ColumnNames = ['Eta', 'Preview']


class ShiftModel(Model):
    """
    Model displaying a FitH5 file contents.
    """
    RootNode = ShiftRootNode
    ColumnsWithDelegates = [1]


class ShiftListNode(Node):
    """
    Root node for a list of entries
    """

    def _loadChildren(self):
        children = []

        iGroup = self.subject.getIntensityGroup()
        xsocsH5 = iGroup.xsocsH5

        entries = xsocsH5.entries()

        for entry in entries:
            child = ShiftNode(iGroup.getIntensityItem(entry),
                              branchName=entry,
                              nodeName=str(xsocsH5.scan_angle(entry)),
                              subject=self.subject)
            children.append(child)

        return children


class ShiftNode(Node):

    def sizeHint(self, column):
        if column == 1:
            return Qt.QSize(150, 150)
        return super(ShiftNode, self).sizeHint(column)

    def __init__(self, iItem, **kwargs):
        super(ShiftNode, self).__init__(**kwargs)

        self.__item = iItem

        self._setDataInternal(ModelColumns.ValueColumn,
                              self.sizeHint(0),
                              role=Qt.Qt.SizeHintRole)

    def getShift(self):
        """
        Returns the shift values for this node.
        :return:
        """
        return self.subject.getShift(self.branchName)

    def getControlPoint(self):
        """
        Returns the coordinates of the selected point, with this node's shift
        applied, or None if the selected point has not been set.
        :return:
        """
        refPoint = self.subject.getReferenceControlPoint()

        if refPoint is None:
            return None

        thisPoint = np.array([refPoint['x'], refPoint['y']])

        refEntry = refPoint['entry']

        if refPoint['entry'] != self.getEntry():
            shift = self.getShift()
            refShift = self.subject.getShift(refEntry)
            thisPoint += [shift.dx - refShift.dx, shift.dy - refShift.dy]

        return thisPoint

    def getEntry(self):
        """
        Returns this node's entry.
        :return:
        """
        return self.branchName

    def getRoiData(self, shifted=True, centered=False):
        """
        Returns this node's intensity data inside the ROI.
        :param shifted: True to apply shift to the ROI.
        :param centered: True to return a ROI with the same shape
            but centered on the selected point.
        :return:
        """
        # TODO : cache the values
        roiData = self.subject.getRoiData(self.branchName,
                                          shifted=shifted,
                                          centered=centered)
        return roiData

    def getFullData(self):
        """
        Returns this node's intensity data.
        :return:
        """
        # TODO : cache the values
        fullData = self.subject.getFullData(self.branchName)
        return fullData

    def subjectSignals(self, column):
        if column == ModelColumns.ValueColumn:
            return [self.subject.sigRoiChanged,
                    self.subject.sigReferenceControlPointChanged,
                    self.subject.sigShiftChanged]
        return []

    def filterEvent(self, column, event):
        if column == ModelColumns.ValueColumn:
            accept = True
            if event:
                if event.signalId == 2 and event.args:
                    accept = event.args[0].entry == self.branchName
            return accept, event
        return super(ShiftNode, self).filterEvent(column, event)

    def pullModelData(self, column, event=None, force=False):
        # TODO : error checking
        self._setIsDirty(True)
        return ModelDataList(None, forceNotify=True)

    def _drawNode(self):
        """
        Draws this node's thumbnail.
        :return:
        """
        rc = super(ShiftNode, self)._drawNode()

        plot = PlotGrabber()
        plot.setFixedSize(Qt.QSize(150, 150))

        # WARNING, DO NOT REMOVE
        # on some systems this is necessary, otherwise the widget is not
        # resized
        # ============
        plot.toPixmap()
        # ============
        # WARNING END

        entry = self.branchName

        shiftSubject = self.subject

        roiData = self.getRoiData(shifted=True, centered=True)

        if roiData is not None:
            pos_0, pos_1, data = roiData.x, roiData.y, roiData.z

            origin = shiftSubject.getReferenceControlPoint()
            shift = shiftSubject.getShift(entry)

            shiftX = shift.dx
            shiftY = shift.dy

            centerX = origin['x'] + shiftX
            centerY = origin['y'] + shiftY
            plot.selectPoint(centerX, centerY)

            plot.setPlotData(pos_0, pos_1, data)

            plot.replot()

        pixmap = plot.toPixmap()

        self._setDataInternal(1, pixmap, Qt.Qt.DecorationRole, notify=True)

        return True and rc


class ShiftSelectorPanel(Qt.QWidget):
    sigSelectionChanged = Qt.Signal(object)

    def __init__(self, shiftSubject, parent=None):
        super(ShiftSelectorPanel, self).__init__(parent)

        layout = Qt.QGridLayout(self)

        self.__tree = tree = ShiftTree(shiftSubject)
        self.__cBox = cBox = Qt.QComboBox()
        self.__refreshBn = refreshBn = Qt.QPushButton()
        self.__autoRefreshCb = autoRefreshCb = Qt.QCheckBox('Auto refresh')
        icon = Qt.QApplication.instance().style().standardIcon(
            Qt.QStyle.SP_BrowserReload)
        refreshBn.setIcon(icon)

        cBox.setModel(tree.model())
        cBox.setRootModelIndex(tree.rootIndex())
        cBox.setCurrentIndex(0)

        bnLayout = Qt.QHBoxLayout()
        bnLayout.addWidget(cBox)
        bnLayout.addWidget(refreshBn)
        bnLayout.addWidget(autoRefreshCb)

        layout.addLayout(bnLayout, 0, 0, Qt.Qt.AlignCenter)
        layout.addWidget(tree, 1, 0)

        tree.sigCurrentChanged.connect(self.__notifySelectionChanged)
        cBox.currentIndexChanged[int].connect(self.__slotIndexChanged)

        shiftSubject.sigShiftChanged.connect(self.__slotSubjectChanged)
        shiftSubject.sigRoiChanged.connect(self.__slotSubjectChanged)
        shiftSubject.sigReferenceControlPointChanged.connect(
            self.__slotSubjectChanged)

        refreshBn.clicked.connect(self.__slotRefreshClicked)

        autoRefreshCb.setChecked(not tree.isAutoRefresh())

        autoRefreshCb.clicked.connect(self.__slotAutoRefreshClicked)

    def __slotAutoRefreshClicked(self, checked):
        """
        Slot called when the auto refresh button is unchecked/checked
        :param checked:
        :return:
        """
        self.__tree.setAutoRefresh(not checked)
        if checked:
            self.__tree.drawDelayedItems()

    def __slotSubjectChanged(self):
        """
        Called when the state of the subject changes.
        :return:
        """
        self.__refreshBn.setEnabled(True)

    def __slotRefreshClicked(self):
        """
        Called when the refresh button is clicked
        :return:
        """
        self.__tree.drawDelayedItems()

    def __slotIndexChanged(self, index):
        """
        Slot called when the index of the combox box changes
        :param index:
        :return:
        """
        mIndex = self.__cBox.rootModelIndex().child(index, 0)
        self.__tree.selectionModel().setCurrentIndex(
            mIndex, Qt.QItemSelectionModel.ClearAndSelect)

    def __notifySelectionChanged(self, node=None):
        """
        Emits the signal sigSelectionChanged
        :param node:
        :return:
        """
        if node is None:
            index = self.__tree.selectionModel().currentIndex()
            node = index.data(role=ModelRoles.InternalDataRole)

        if node is None:
            # this shouldnt happen ever
            raise ValueError('Node is none')

        self.sigSelectionChanged.emit(node)

    def treeView(self):
        """
        Returns the TreeView
        :return:
        """
        return self.__tree

    def comboBox(self):
        """
        Returns the QComboBox
        :return:
        """
        return self.__cBox


class ShiftPlotWidget(Qt.QWidget):
    sigSelectionChange = Qt.Signal(object)
    """ Signal emitted when the current selected entry changes """

    sigPointSelected = Qt.Signal(object, object)
    """ Signal emitted when a point is selected on the plot
        Parameters are node and selected coordinates XsocsPlot2DPoint """

    def __init__(self,
                 shiftModel,
                 rootIndex,
                 applyRoi=False,
                 showSelection=False,
                 parent=None):
        """
        A widget containing a Plot and a combobox for entry selection.
        :param shiftModel: The shiftModel to use.
        :param rootIndex: The root index for the combobox.
        :param applyRoi: True to apply the ROI to the displayed data.
        :param showSelection: True to display the selected point (crosshair).
        :param parent:
        """
        super(ShiftPlotWidget, self).__init__(parent)

        layout = Qt.QGridLayout(self)

        self.__cBox = cBox = Qt.QComboBox()
        self.__entryLocked = False

        self.__applyRoi = applyRoi
        self.__showSelection = showSelection

        cBox.setModel(shiftModel)
        cBox.setRootModelIndex(rootIndex)
        cBox.setCurrentIndex(0)

        layout.addWidget(cBox, 0, 0, Qt.Qt.AlignCenter)

        self.__plot = plot = XsocsPlot2D()
        self.__plot.setSnapToPoint(True)
        plot.setPointSelectionEnabled(True)

        layout.addWidget(plot, 1, 0, Qt.Qt.AlignCenter)

        cBox.currentIndexChanged[int].connect(self.__slotIndexChanged)
        plot.sigPointSelected.connect(self.__slotPointSelected)

        cBox.setCurrentIndex(0)
        cBox.currentIndexChanged.emit(0)

    def lockEntry(self, lock):
        """
        Locks the entry selector to the current entry.
        :param lock:
        :return:
        """
        self.__entryLocked = lock
        self.__cBox.setEnabled(not lock)

    def __slotPointSelected(self, point):
        """
        Slot called when a point is selected on the plot.
        :param point:
        :return:
        """
        node = self.__getCurrentNode()
        self.sigPointSelected.emit(node, point)

    def plot(self):
        """
        Returns the plot widget.
        :return:
        """
        return self.__plot

    def setSnapToPoint(self, snap):
        """
        Set the snapToPoint flag of the plot.
        :param snap:
        :return:
        """
        self.__plot.setSnapToPoint(snap)

    def __slotIndexChanged(self, index):
        """
        Slot called when the index of the combox box changes
        :param index:
        :return:
        """
        mIndex = self.__cBox.rootModelIndex().child(index, 0)
        node = mIndex.data(role=ModelRoles.InternalDataRole)

        if node is None:
            # this shouldnt happen ever
            raise ValueError('Node is none')

        self.setCurrentModelIndex(mIndex)

        self.sigSelectionChange.emit(node)

    def comboBox(self):
        """
        Returns the QComboBox
        :return:
        """
        return self.__cBox

    def getCurrentEntry(self):
        """
        Returns the current selected entry.
        :return:
        """
        node = self.__getCurrentNode()
        return node.branchName

    def setCurrentModelIndex(self, mdlIndex):
        """
        Sets the current displayed node to the one referenced by the given
        QModelIndex (shiftModel index).
        :param mdlIndex:
        :return:
        """

        plot = self.__plot
        plot.clear()

        if not mdlIndex.isValid():
            raise ValueError('Invalid index.')

        node = mdlIndex.data(ModelRoles.InternalDataRole)

        self.__cBox.setCurrentIndex(mdlIndex.row())

        self.__drawPlotData(node)
        self.__drawSelectedPoint(node)

    def __getCurrentNode(self):
        cBox = self.__cBox
        mIndex = cBox.rootModelIndex().child(cBox.currentIndex(), 0)
        node = mIndex.data(role=ModelRoles.InternalDataRole)

        if node is None:
            # this shouldnt happen ever
            raise ValueError('Node is none')

        return node

    def __drawPlotData(self, node=None):
        """
        Draws the scatter plot
        :return:
        """
        if node is None:
            node = self.__getCurrentNode()

        if self.__applyRoi:
            roiData = node.getRoiData(shifted=False)
        else:
            roiData = node.getFullData()

        if roiData is not None:
            self.__plot.setPlotData(roiData.x,
                                    roiData.y,
                                    roiData.z,
                                    dataIndices=roiData.idx)

    def __drawSelectedPoint(self, node=None):
        """
        Draws the selected point crosshair
        :return:
        """

        if not self.__showSelection:
            return

        if node is None:
            node = self.__getCurrentNode()

        selectedPt = node.getControlPoint()
        if selectedPt is not None:
            self.__plot.selectPoint(*selectedPt)

    def refreshPlotData(self):
        """
        Redraws the plot data
        :return:
        """
        self.__drawPlotData()

    def refreshSelectedPoint(self):
        """
        Redraws the selected point crosshair
        :return:
        """
        self.__drawSelectedPoint()


class ShiftTree(TreeView):
    sigCurrentChanged = Qt.Signal(object)

    def __init__(self, shiftSubject, **kwargs):
        super(ShiftTree, self).__init__(**kwargs)

        shiftList = ShiftListNode(subject=shiftSubject)
        model = ShiftModel()
        model.appendGroup(shiftList)
        self.setModel(model)
        self.setShowUniqueGroup(True)
        self.setRootIndex(shiftList.index())
        model.startModel()

    def currentChanged(self, current, previous):
        super(ShiftTree, self).currentChanged(current, previous)
        node = current.data(ModelRoles.InternalDataRole)
        if not node:
            return
        self.sigCurrentChanged.emit(node)


class ShiftAreaSelectorWidget(Qt.QWidget):
    """
    A ShiftPlotWidget + a roi widget.
    """

    sigRoiApplied = Qt.Signal(object, object)
    """ Triggered when the ROI is applied. Args are : the roi, and the node
        on which the ROI was applied.
    """

    def __init__(self,
                 shiftModel,
                 rootIndex,
                 **kwargs):
        super(ShiftAreaSelectorWidget, self).__init__(**kwargs)

        layout = Qt.QGridLayout(self)

        self.__firstShow = True

        self.__plotWid = plotWid = ShiftPlotWidget(shiftModel,
                                                   rootIndex)
        plotWid.plot().setPointSelectionEnabled(False)
        self.__roiWidget = roiWidget = RectRoiWidget(plot=plotWid.plot())
        roiWidget.applyButton().setText('View selection')

        layout.addWidget(plotWid, 0, 0)
        layout.addWidget(roiWidget, 1, 0,
                         Qt.Qt.AlignTop | Qt.Qt.AlignCenter)

        plotWid.sigSelectionChange.connect(self.__slotSelectionChanged)
        roiWidget.sigRoiApplied.connect(self.__slotRoiApplied)

        plotWid.comboBox().setCurrentIndex(0)
        plotWid.comboBox().currentIndexChanged.emit(0)

    def __slotSelectionChanged(self, node):
        """
        Slot Called when the selection changes in the ShitPlotWidget combobox.
        :param node:
        :return:
        """
        plot = self.__plotWid.plot()
        plot.clear()

        if not node:
            # this shouldnt happen ever
            raise ValueError('node is None')

        plotData = node.getFullData()

        plot.setPlotData(plotData.x, plotData.y, values=plotData.z)

    def showEvent(self, event):
        super(ShiftAreaSelectorWidget, self).showEvent(event)
        if self.__firstShow:
            self.__firstShow = False
            self.__plotWid.plot().resetZoom()

    def __slotRoiApplied(self, roi):
        """
        Slot called when the ROI is applied.
        Emits ShiftAreaSelectorWidget.sigRoiApplied
        :param roi:
        :return:
        """
        cbox = self.__plotWid.comboBox()
        mIndex = cbox.rootModelIndex().child(cbox.currentIndex(), 0)

        if not mIndex.isValid():
            # THIS SHOULD NEVER HAPPEN
            raise ValueError('Invalid index')

        node = mIndex.data(ModelRoles.InternalDataRole)

        self.sigRoiApplied.emit(roi, node)


class ShiftPreviewPanel(Qt.QWidget):
    def __init__(self,
                 shiftSubject,
                 parent=None,
                 **kwargs):
        super(ShiftPreviewPanel, self).__init__(parent, **kwargs)

        self.__subject = shiftSubject

        layout = Qt.QGridLayout(self)

        self.__snapToGrid = True

        self.__refreshBn = refreshBn = Qt.QPushButton('Refresh')
        icon = Qt.QApplication.instance().style().standardIcon(
            Qt.QStyle.SP_BrowserReload)
        refreshBn.setIcon(icon)

        self.__progBar = progBar = Qt.QProgressBar()

        self.__plot = plot = XsocsPlot2D()

        layout.addWidget(refreshBn, 0, 0, Qt.Qt.AlignCenter)
        layout.addWidget(progBar, 0, 1, Qt.Qt.AlignCenter)
        layout.addWidget(plot, 2, 0, 1, 2)

        shiftSubject.sigShiftChanged.connect(self.__slotShiftChanged)
        refreshBn.clicked.connect(self.__slotRefreshClicked)

    def setSnapToGrid(self, snap):
        """
        Sets the snapToGrid to True (called by ShiftView when checking
        the regular grid) checkbox.
        :param snap:
        :return:
        """
        if snap != self.__snapToGrid:
            self.__snapToGrid = snap
            self.__slotShiftChanged()

    def __slotShiftChanged(self):
        """
        Called when a shift value changes. Notifies the user
        that a refresh is needed.
        :return:
        """
        plot = self.__plot
        plot.clear()
        self.__refreshBn.setEnabled(True)

    # def __setProgress(self, value, text):
    #     """
    #     Sets the value and text of the progress bar
    #     :param value:
    #     :param text:
    #     :return:
    #     """
    #     self.__progBar.setValue(value)

    def __slotRefreshClicked(self):
        """
        Called when the refresh button is clicked
        :return:
        """
        intersectionPoints = self.__subject.getIntersectionIndices(
            grid=self.__snapToGrid,
            progressCb=self.__progBar.setValue)
        self.__refreshBn.setEnabled(False)

        self.__plot.clear()

        if intersectionPoints is None or intersectionPoints.size == 0:
            return

        # xsocsH5 = self.__subject.xsocsH5()
        # entries = xsocsH5.entries()
        # scan_positions = xsocsH5.scan_positions(entries[0])

        iGroup = self.__subject.getIntensityGroup()
        total = iGroup.getIntensityItem('Total')
        intensity, scan_positions = total.getScatterData()

        # data = np.zeros(scan_positions.pos_0.size)
        data = np.full(scan_positions.pos_0.size, np.nan)
        data[intersectionPoints] = intensity[intersectionPoints]

        self.__plot.setPlotData(scan_positions.pos_0,
                                scan_positions.pos_1,
                                values=data)

        title = 'Points : {0}/{1}'.format(intersectionPoints.size,
                                          scan_positions.pos_0.size)
        self.__plot.setGraphTitle(title)


class ShiftWidget(Qt.QMainWindow):
    """
    Window allowing a user to display/set the shift
    (sample holder displacement).
    """

    def __init__(self,
                 intensityGroup,
                 shiftItem,
                 parent=None,
                 **kwargs):
        super(ShiftWidget, self).__init__(parent, **kwargs)

        self.setWindowTitle('[XSOCS] Shift {0}:{1}'.format(intensityGroup.filename,
                                                     intensityGroup.path))

        centralWidget = Qt.QWidget()
        centralLayout = Qt.QGridLayout(centralWidget)
        self.setCentralWidget(centralWidget)

        self.__shiftSubject = shiftSubject = ShiftSubject(intensityGroup,
                                                          shiftItem)
        xsocsH5 = intensityGroup.xsocsH5

        # Preview panel
        self.__selector = selector = ShiftSelectorPanel(shiftSubject)
        selSubGroup = GroupBox('Preview')
        layout = Qt.QVBoxLayout(selSubGroup)
        layout.addWidget(selector)

        treeView = selector.treeView()

        # Point preview
        self.__previewPanel = previewPanel = ShiftPreviewPanel(
            shiftSubject=shiftSubject)
        pointsSubGroup = GroupBox('Points')
        layout = Qt.QVBoxLayout(pointsSubGroup)
        layout.addWidget(previewPanel)

        # tab widget for the previews
        # self.__previewTab = tabWidget = Qt.QTabWidget()
        # tabWidget.addTab(selector, 'Entries')
        # tabWidget.addTab(previewPanel, 'Points')
        entriesDock = Qt.QDockWidget('Entries', self)
        entriesDock.setWidget(selSubGroup)
        features = entriesDock.features() ^ Qt.QDockWidget.DockWidgetClosable
        entriesDock.setFeatures(features)
        self.addDockWidget(Qt.Qt.RightDockWidgetArea, entriesDock)

        pointsDock = Qt.QDockWidget('Points', self)
        pointsDock.setWidget(pointsSubGroup)
        features = pointsDock.features() ^ Qt.QDockWidget.DockWidgetClosable
        pointsDock.setFeatures(features)
        # self.addDockWidget(Qt.Qt.RightDockWidgetArea, dock)
        self.tabifyDockWidget(entriesDock, pointsDock)
        entriesDock.setTitleBarWidget(Qt.QWidget())
        pointsDock.setTitleBarWidget(Qt.QWidget())
        entriesDock.raise_()

        self.setTabPosition(
            Qt.Qt.AllDockWidgetAreas, Qt.QTabWidget.North)

        # options
        isRegular = True
        # optionsSubGroup = GroupBox('Options')
        # layout = Qt.QVBoxLayout(optionsSubGroup)
        self.__snapBn = snapBn = Qt.QCheckBox('Grid shift.')
        # layout.addWidget(snapBn)
        # isRegular = xsocsH5.is_regular_grid(xsocsH5.entries()[0])
        # snapBn.setEnabled(isRegular)
        snapBn.setChecked(isRegular)

        # "Ref" panel
        self.__refPanel = refPanel = ShiftPlotWidget(treeView.model(),
                                                     treeView.rootIndex(),
                                                     applyRoi=True,
                                                     showSelection=True)
        refPanel.setSnapToPoint(isRegular)
        refPanel.lockEntry(True)
        refSubGroup = GroupBox('Reference')
        layout = Qt.QVBoxLayout(refSubGroup)
        layout.addWidget(refPanel)
        refPanel.plot().setPointSelectionEnabled(True)

        # "Set" panel
        self.__setPanel = setPanel = ShiftPlotWidget(treeView.model(),
                                                     treeView.rootIndex(),
                                                     applyRoi=True,
                                                     showSelection=True)
        setPanel.setSnapToPoint(isRegular)
        setPanel.plot().setPointSelectionEnabled(True)

        linearShiftBn = Qt.QPushButton('Apply linear shift')
        self.__linearShiftBn = linearShiftBn
        resetShiftBn = Qt.QPushButton('Reset entry shift')
        resetAllShiftBn = Qt.QPushButton('Reset ALL shifts')
        linearShiftBn.setEnabled(False)
        # resetShiftBn.setEnabled(False)
        # resetAllShiftBn.setEnabled(False)
        setSubGroup = GroupBox('Set')
        layout = Qt.QVBoxLayout(setSubGroup)
        layout.addWidget(setPanel)

        bnBase = Qt.QWidget()
        bnBase.setContentsMargins(0, 0, 0, 0)
        bnLayout = Qt.QHBoxLayout(bnBase)
        bnLayout.addWidget(linearShiftBn, alignment=Qt.Qt.AlignCenter)
        bnLayout.addWidget(resetShiftBn, alignment=Qt.Qt.AlignCenter)
        bnLayout.addWidget(resetAllShiftBn, alignment=Qt.Qt.AlignCenter)
        layout.addWidget(bnBase, alignment=Qt.Qt.AlignCenter)

        # ROI panel
        self.__areaPlot = areaPlot = ShiftAreaSelectorWidget(
            treeView.model(), treeView.rootIndex())
        roiSubGroup = GroupBox('Area selection')
        layout = Qt.QVBoxLayout(roiSubGroup)
        layout.addWidget(areaPlot)

        # X and Y shif plot
        self.__xShiftPlot = xShiftPlot = XsocsPlot2D()
        self.__yShiftPlot = yShiftPlot = XsocsPlot2D()
        xShiftPlot.setGraphXLabel('entry #')
        xShiftPlot.setGraphYLabel('hirizontal shift')
        xShiftPlot.setGraphYLabel('column shift', axis='right')
        yShiftPlot.setGraphXLabel('entry #')
        yShiftPlot.setGraphYLabel('vertical shift')
        yShiftPlot.setGraphYLabel('row shift', axis='right')
        xShiftPlot.setKeepDataAspectRatio(False)
        yShiftPlot.setKeepDataAspectRatio(False)
        shiftSubGroup = GroupBox('Shift/Entry')
        layout = Qt.QHBoxLayout(shiftSubGroup)
        layout.addWidget(xShiftPlot)
        layout.addWidget(yShiftPlot)

        buttonsLayout = Qt.QHBoxLayout()
        buttons = Qt.QDialogButtonBox.Save | Qt.QDialogButtonBox.Cancel
        bottomBnGrp = Qt.QDialogButtonBox(buttons)
        self.__saveProgBar = saveProgBar = Qt.QProgressBar()

        buttonsLayout.addWidget(bottomBnGrp)
        buttonsLayout.addWidget(saveProgBar)

        centralLayout.addWidget(roiSubGroup, 0, 0, 4, 1)
        # centralLayout.addWidget(optionsSubGroup, 0, 1)
        centralLayout.addWidget(refSubGroup, 0, 1)
        centralLayout.addWidget(setSubGroup, 1, 1)
        # centralLayout.addWidget(tabWidget, 0, 2, 3, 1)
        centralLayout.addWidget(shiftSubGroup, 2, 1)
        centralLayout.addLayout(buttonsLayout, 3, 1, Qt.Qt.AlignLeft)

        # signal triggered when the ROI is applied in the full plot window
        areaPlot.sigRoiApplied.connect(self.__slotRoiApplied)

        # signal triggered when an entry is selected in the preview widget
        selector.sigSelectionChanged.connect(self.__slotSelectorChanged)

        # signal triggered when an entry is selected in the ref panel
        # refPanel.sigSelectionChange.connect(self.__slotRefSelectionChanged)

        # signal triggered when an entry is selected in the set panel
        setPanel.sigSelectionChange.connect(self.__slotSelectorChanged)

        # signal triggered when a point is selected on the REF plot
        refPanel.sigPointSelected.connect(self.__slotRefPointSelected)

        # signal triggered when a point is selected on the SET plot
        setPanel.sigPointSelected.connect(self.__slotSetPointSelected)

        # signal triggered when a shift value is changed
        self.__shiftSubject.sigShiftChanged.connect(self.__updateShiftPlot)

        linearShiftBn.clicked.connect(self.__slotLinearShiftClicked)
        resetShiftBn.clicked.connect(self.__slotResetShiftClicked)
        resetAllShiftBn.clicked.connect(self.__slotResetAllShiftClicked)

        snapBn.toggled.connect(self.__slotSnapBnToggled)

        bottomBnGrp.rejected.connect(self.close)

        bottomBnGrp.accepted.connect(self.__slotAccepted)

        self.__updateShiftPlot()

    def model(self):
        """
        Returns the ShiftModel
        :return:
        """
        return self.__selector.treeView().model()

    def __slotAccepted(self):
        """
        Slot called when the "save" button is clicked
        :return:
        """
        self.__shiftSubject.writeToShiftH5(
            isSnapped=self.__snapBn.isChecked(),
            progressCb=self.__saveProgBar.setValue)
        self.close()

    def __slotResetAllShiftClicked(self):
        """
        Slot called when the "reset all shifts" button is clicked
        :return:
        """
        buttons = Qt.QMessageBox.Yes | Qt.QMessageBox.Cancel
        ans = Qt.QMessageBox.question(self,
                                      'Reset all?',
                                      'Are you sure you want to reset all '
                                      'shifts to 0?',
                                      buttons=buttons,
                                      defaultButton=Qt.QMessageBox.Cancel)

        if ans == Qt.QMessageBox.Cancel:
            return

        self.__shiftSubject.resetShifts()

    def __slotResetShiftClicked(self):
        """
        Slot called when the "reset enty shift" button is clicked
        :return:
        """
        subject = self.__shiftSubject
        setEntry = self.__setPanel.getCurrentEntry()

        buttons = Qt.QMessageBox.Ok | Qt.QMessageBox.Cancel
        ans = Qt.QMessageBox.question(self,
                                      'Reset shift?',
                                      'Reset shift to 0 for entry :\n'
                                      '{0}?'
                                      ''.format(setEntry),
                                      buttons=buttons,
                                      defaultButton=Qt.QMessageBox.Cancel)
        if ans == Qt.QMessageBox.Cancel:
            return

        subject.setShift(setEntry, 0., 0.)

    def __slotLinearShiftClicked(self):
        """
        Slot called when the "Apply linear shift" button is clicked
        :return:
        """
        refEntry = self.__refPanel.getCurrentEntry()
        setEntry = self.__setPanel.getCurrentEntry()

        if refEntry == setEntry:
            Qt.QMessageBox.warning(self,
                                   'Same entry',
                                   'Can\'t set the shift, '
                                   'ref and set entries are the same.')
            return

        subject = self.__shiftSubject

        buttons = Qt.QMessageBox.Ok | Qt.QMessageBox.Cancel
        ans = Qt.QMessageBox.question(self,
                                      'Apply linear shift',
                                      'Are you sure you want to set a linear '
                                      'shift between entries\n'
                                      '- {0}\n'
                                      'and\n'
                                      '- {1}?'
                                      ''.format(refEntry, setEntry),
                                      buttons=buttons,
                                      defaultButton=Qt.QMessageBox.Cancel)
        if ans == Qt.QMessageBox.Cancel:
            return

        subject.applyLinearShift(setEntry, refEntry)
        self.__updateShiftPlot()

    def __slotSnapBnToggled(self, checked):
        """
        Slot called when the "snap to grid" checkbox is toggled.
        :param checked:
        :return:
        """
        self.__setPanel.setSnapToPoint(checked)
        self.__refPanel.setSnapToPoint(checked)
        self.__previewPanel.setSnapToGrid(checked)
        self.__updateShiftPlot()

    def __updateShiftPlot(self):
        """
        Updates the shift plot
        :return:
        """
        shifts = self.__shiftSubject.getShifts()
        nShifts = len(shifts)

        if self.__snapBn.isChecked():
            dx = np.ndarray(nShifts, dtype=np.int32)
            dy = np.ndarray(nShifts, dtype=np.int32)

            for iShift, shift in enumerate(shifts):
                grid_shift = shift.grid_shift
                if grid_shift is None:
                    grid_shift = [0, 0]
                dx[iShift] = grid_shift[0]
                dy[iShift] = grid_shift[1]

            self.__xShiftPlot.addCurve(np.arange(dx.size),
                                       dx,
                                       color='red',
                                       legend='gridshift_dx',
                                       yaxis='right',
                                       linestyle=' ',
                                       symbol='s')
            self.__yShiftPlot.addCurve(np.arange(dy.size),
                                       dy,
                                       color='red',
                                       legend='gridshift_dy',
                                       yaxis='right',
                                       linestyle=' ',
                                       symbol='s')

        dx = np.ndarray(nShifts, dtype=np.double)
        dy = np.ndarray(nShifts, dtype=np.double)

        for iShift, shift in enumerate(shifts):
            dx[iShift] = shift.dx
            dy[iShift] = shift.dy

        self.__xShiftPlot.addCurve(np.arange(dx.size),
                                   dx,
                                   color='blue',
                                   legend='shift_dx',
                                   linestyle=' ',
                                   symbol='o')
        self.__yShiftPlot.addCurve(np.arange(dy.size),
                                   dy,
                                   color='blue',
                                   legend='shift_dy',
                                   linestyle=' ',
                                   symbol='o')

    def __slotRefPointSelected(self, node, point):
        """
        Slot called when a point is selected on the REF plot.
        Updates the model's "SelectedPoint".
        :param point:
        :return:
        """
        self.__shiftSubject.setReferenceControlPoint(node.getEntry(),
                                                     point.x,
                                                     point.y,
                                                     point.xIdx)
        self.__setPanel.refreshSelectedPoint()

    def __slotSetPointSelected(self, node, point):
        """
        Slot called when a point is selected on the REF plot.
        Updates the model's "SelectedPoint".
        :param point:
        :return:
        """
        setEntry = node.getEntry()

        try:
            self.__shiftSubject.setControlPoint(setEntry,
                                                point.x,
                                                point.y,
                                                point.xIdx)
        except ValueError as ex:
            Qt.QMessageBox.warning(self, 'Exception', str(ex))
            return

    def __slotSelectorChanged(self, node):
        """
        Slot called when the "set" selection changes.
        Called from the preview panel or the "set" combobox.
        :param node:
        :return:
        """
        self.__setPanel.setCurrentModelIndex(node.index())

    def __slotRoiApplied(self, roi, node):
        """
        Slot called when the ROI in the ShiftAreaSelectorWidget is applied.
        :param node: the node that was selected in the Roi selection widget
            when the roi was applied.
        :param roi: the roi values
        :return:
        """
        mdlIndex = node.index()
        self.__shiftSubject.setRoi(roi)
        self.__refPanel.setCurrentModelIndex(mdlIndex)
        self.__setPanel.setCurrentModelIndex(mdlIndex)
        self.__linearShiftBn.setEnabled(True)


if __name__ == '__main__':
    pass
